use std::{env, fs};
use std::fs::File;
use std::io::{self, BufRead};

pub fn get_input() -> io::Result<Vec<String>> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        return Err(io::Error::from(io::ErrorKind::InvalidInput));
    }

    let file = File::open(&args[1])?;
    io::BufReader::new(file).lines().collect()
}

pub fn get_inputs() -> io::Result<Vec<Vec<String>>> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        return Err(io::Error::from(io::ErrorKind::InvalidInput));
    }

    Ok(fs::read_to_string(&args[1])?
        .split("\n\n")
        .map(|s| s.split("\n").map(|s| s.to_string()).collect::<Vec<_>>())
        .collect())
}
