use aoc::get_input;
use std::collections::{HashSet, VecDeque};


struct Droplet {
    x: usize,
    y: usize,
    z: usize,
}

impl Droplet {
    fn new(input: String) -> Self {
        let droplet = input.split(",").map(|v| v.parse().unwrap()).collect::<Vec<_>>();
        let x = droplet[0];
        let y = droplet[1];
        let z = droplet[2];

        Droplet { x, y, z }
    }
}

struct Lava {
    shape: Vec<Vec<Vec<bool>>>,
}

impl Lava {
    fn new(droplets: Vec<Droplet>) -> Self {
        let max_x = droplets.iter().map(|d| d.x).max().unwrap() + 3;
        let max_y = droplets.iter().map(|d| d.y).max().unwrap() + 3;
        let max_z = droplets.iter().map(|d| d.z).max().unwrap() + 3;

        let mut shape = Vec::from((0..max_x).map(|_| Vec::from((0..max_y).map(|_| Vec::from((0..max_z).map(|_| false).collect::<Vec<_>>())).collect::<Vec<_>>())).collect::<Vec<_>>());

        for droplet in droplets {
            shape[droplet.x + 1][droplet.y + 1][droplet.z + 1] = true;
        }

        Lava { shape }
    }

    fn count(&self) -> usize {
        type XYZ = (usize, usize, usize);

        let mut res = 0;

        let lx = self.shape.len();
        let ly = self.shape[0].len();
        let lz = self.shape[0][0].len();

        let mut visited: HashSet<XYZ> = HashSet::new();

        let mut stack: VecDeque<XYZ> = VecDeque::from([ (0, 0, 0) ]);
        while !stack.is_empty() {
            let cube = stack.pop_front().unwrap();
            if visited.insert(cube) {
                let (x, y, z) = cube;
                if x < (lx - 1) {
                    if self.shape[x + 1][y][z] {
                        res += 1;
                    } else {
                        stack.push_back((x + 1, y, z));
                    }
                }
                if x > 0 {
                    if self.shape[x - 1][y][z] {
                        res += 1;
                    } else {
                        stack.push_back((x - 1, y, z));
                    }
                }
                if y < (ly - 1) {
                    if self.shape[x][y + 1][z] {
                        res += 1;
                    } else {
                        stack.push_back((x, y + 1, z));
                    }
                }
                if y > 0 {
                    if self.shape[x][y - 1][z] {
                        res += 1;
                    } else {
                        stack.push_back((x, y - 1, z));
                    }
                }
                if z < (lz - 1) {
                    if self.shape[x][y][z + 1] {
                        res += 1;
                    } else {
                        stack.push_back((x, y, z + 1));
                    }
                }
                if z > 0 {
                    if self.shape[x][y][z - 1] {
                        res += 1;
                    }     else {
                        stack.push_back((x, y, z -1));
                    }
                }
            }
        }
        res
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let droplets = lines.into_iter()
        .map(|input| Droplet::new(input)).collect();
    let lava = Lava::new(droplets);

    println!("{}", lava.count());
}