use std::str::FromStr;
use aoc::get_input;

#[derive(PartialEq, Copy, Clone)]
enum Move {
    Rock,
    Paper,
    Scissors,
}

impl FromStr for Move {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" => Ok(Move::Rock),
            "B" => Ok(Move::Paper),
            "C" => Ok(Move::Scissors),
            _ => Err(()),
        }
    }
}

impl Move {
    fn shape_score(&self) -> i32 {
        match self {
            Move::Rock => 1,
            Move::Paper => 2,
            Move::Scissors => 3,
        }
    }

    fn outcome_score(&self, response: &Move) -> i32 {
        match (self, response) {
            (Move::Rock, Move::Paper) => 6,
            (Move::Paper, Move::Scissors) => 6,
            (Move::Scissors, Move::Rock) => 6,
            (a, b) if a == b => 3,
            (_, _) => 0,
        }
    }

    fn strategic_response(&self, strategy: &Strategy) -> Move {
        match (strategy, self) {
            (Strategy::Draw, a) => a.clone(),
            (Strategy::Win, Move::Rock) => Move::Paper,
            (Strategy::Win, Move::Paper) => Move::Scissors,
            (Strategy::Win, Move::Scissors) => Move::Rock,
            (Strategy::Lose, Move::Rock) => Move::Scissors,
            (Strategy::Lose, Move::Paper) => Move::Rock,
            (Strategy::Lose, Move::Scissors) => Move::Paper,
        }
    }
}

enum Strategy {
    Lose,
    Draw,
    Win,
}

impl FromStr for Strategy {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "X" => Ok(Strategy::Lose),
            "Y" => Ok(Strategy::Draw),
            "Z" => Ok(Strategy::Win),
            _ => Err(()),
        }
    }
}


fn get_moves(line: String) -> (Move, Move) {
    let prediction = Move::from_str(&line[..1]).unwrap();
    let strategy = Strategy::from_str(&line[2..]).unwrap();
    let response = prediction.strategic_response(&strategy);
    (prediction, response)
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut total_score = 0;

    for line in lines {
        let (prediction, strategy) = get_moves(line);
        total_score += strategy.shape_score() + prediction.outcome_score(&strategy);
    }

    println!("{:?}", total_score);
}