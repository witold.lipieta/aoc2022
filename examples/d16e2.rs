use aoc::get_input;
use std::collections::{HashMap, HashSet, VecDeque};

type Tunnels = HashMap<String, Vec<String>>;
type Distances = HashMap<String, i32>;
type Map = HashMap<String, Distances>;
type FlowRates = HashMap<String, i32>;

#[derive(Debug, Clone)]
struct Status {
    valve: String,
    distance_covered: i32,
}

#[derive(Debug, Clone)]
struct State {
    me: Status,
    elephant: Status,
    reward: i32,
    minutes_left: i32,
    open_valves: HashSet<String>,
}

fn distances(tunnels: &Tunnels, from: String) -> Distances {
    let mut distances: Distances = tunnels
        .iter()
        .map(|(valve, _)| (valve.clone(), 100000))
        .collect();

    let mut stack: VecDeque<(String, i32)> = VecDeque::from([(from, 0)]);
    while !stack.is_empty() {
        let (valve, distance) = stack.pop_front().unwrap();
        if let Some(current_distance) = distances.get_mut(&valve) {
            if *current_distance > distance {
                *current_distance = distance;
                for next_valve in tunnels.get(&valve).unwrap() {
                    stack.push_back((next_valve.clone(), distance + 1));
                }
            }
        }
    }

    distances
}

struct Volcano {
    map: Map,
    flow_rates: FlowRates,
}

impl Volcano {
    fn new(input: Vec<String>) -> Self {
        let mut flow_rates: FlowRates = FlowRates::new();
        let mut tunnels: Tunnels = Tunnels::new();

        for line in input {
            let (valve_s, tunnels_s) = line.split_once("; ").unwrap();
            let (valve, flow_rate) = valve_s
                .strip_prefix("Valve ")
                .unwrap()
                .split_once(" has flow rate=")
                .unwrap();

            let flow_rate = flow_rate.parse().unwrap();
            if flow_rate != 0 || valve == "AA" {
                flow_rates.insert(valve.to_string(), flow_rate);
            }
            if let Some(target_valve) = tunnels_s.strip_prefix("tunnel leads to valve ") {
                tunnels.insert(valve.to_string(), Vec::from([target_valve.to_string()]));
            } else if let Some(target_valves) = tunnels_s.strip_prefix("tunnels lead to valves ") {
                tunnels.insert(
                    valve.to_string(),
                    target_valves
                        .split(", ")
                        .map(|s| s.to_string())
                        .collect::<Vec<_>>(),
                );
            }
        }

        let map: Map = flow_rates
            .iter()
            .map(|(name, _)| {
                (
                    name.clone(),
                    distances(&tunnels, name.clone())
                        .into_iter()
                        .filter(|(v, _)| flow_rates.contains_key(v))
                        .collect(),
                )
            })
            .collect();

        Volcano { map, flow_rates }
    }

    fn possible_states(&self, state: &State) -> Vec<State> {
        state
            .open_valves
            .iter()
            .filter_map(|valve| {
                let my_distance = self.map.get(&state.me.valve).unwrap().get(valve).unwrap()
                    - state.me.distance_covered;
                let elephant_distance = self
                    .map
                    .get(&state.elephant.valve)
                    .unwrap()
                    .get(valve)
                    .unwrap()
                    - state.elephant.distance_covered;
                let minutes_to_open = my_distance.min(elephant_distance) + 1;
                let minutes_left = state.minutes_left - minutes_to_open.max(0);
                if minutes_left < 0 {
                    return None;
                }
                let mut open_valves = state.open_valves.clone();
                open_valves.remove(valve);
                let flow_rate = self.flow_rates.get(valve).unwrap();
                let reward = (minutes_left - minutes_to_open.min(0)) * flow_rate + state.reward;

                if my_distance <= elephant_distance {
                    Some(State {
                        me: Status {
                            valve: valve.clone(),
                            distance_covered: minutes_to_open.min(0).abs(),
                        },
                        elephant: Status {
                            valve: state.elephant.valve.clone(),
                            distance_covered: minutes_to_open.max(0),
                        },
                        reward,
                        minutes_left,
                        open_valves,
                    })
                } else {
                    Some(State {
                        me: Status {
                            valve: state.me.valve.clone(),
                            distance_covered: minutes_to_open.max(0),
                        },
                        elephant: Status {
                            valve: valve.clone(),
                            distance_covered: minutes_to_open.min(0).abs(),
                        },
                        reward,
                        minutes_left,
                        open_valves,
                    })
                }
            })
            .collect()
    }
}

fn recursive(volcano: &Volcano, start_state: &State) -> i32 {
    match volcano
        .possible_states(&start_state)
        .iter()
        .map(|state| recursive(volcano, state))
        .max()
    {
        None => start_state.reward,
        Some(max) => max,
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let volcano = Volcano::new(lines);
    let start_state = State {
        me: Status {
            valve: "AA".to_string(),
            distance_covered: 0,
        },
        elephant: Status {
            valve: "AA".to_string(),
            distance_covered: 0,
        },
        reward: 0,
        minutes_left: 26,
        open_valves: volcano
            .flow_rates
            .iter()
            .filter_map(|(valve, flow_rate)| (*flow_rate != 0).then(|| valve.clone()))
            .collect(),
    };

    println!("{:?}", recursive(&volcano, &start_state));
}
