use aoc::get_input;

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut calories = 0;
    let mut elves: Vec<i32> = Vec::new();

    for line in lines {
        if line.is_empty() {
            elves.push(calories);
            calories = 0;
        } else {
            calories += line.parse::<i32>().unwrap();
        }
    }
    elves.push(calories);

    elves.sort();
    let most_calories: i32 = elves[elves.len()-3..].iter().sum();
    println!("{:?}", most_calories);
}
