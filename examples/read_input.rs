use aoc::get_input;

// run example with cargo run --example read_input data/ti0
fn main() {
    let lines: Vec<String> = get_input().unwrap();

    println!("{:?}", lines);
}