use crate::Sand::{Falls, Stops, Voids};
use aoc::get_input;
use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::hash::Hash;

enum Structure {
    Rock,
    Sand,
}

#[derive(Hash, Eq, PartialEq)]
struct Position {
    x: usize,
    y: usize,
}

impl Position {
    fn new(input: &str) -> Self {
        input
            .split_once(",")
            .map(|(x, y)| Position {
                x: x.parse().unwrap(),
                y: y.parse().unwrap(),
            })
            .unwrap()
    }
}

struct Scan {
    slice: HashMap<Position, Structure>,
    floor_level: usize,
}

enum Sand {
    Stops,
    Falls(Position),
    Voids,
}

impl Scan {
    fn new() -> Self {
        Scan {
            slice: HashMap::new(),
            floor_level: 0,
        }
    }

    fn add_path(&mut self, points: Vec<Position>) {
        let lines = points.len() - 1;
        let mut path: Vec<Position> = Vec::new();
        for i in 0..lines {
            let start = &points[i];
            let end = &points[i + 1];
            let x_min = start.x.min(end.x);
            let x_max = start.x.max(end.x);
            let y_min = start.y.min(end.y);
            let y_max = start.y.max(end.y);

            self.floor_level = self.floor_level.max(y_max + 2);

            path.extend(
                (x_min..(x_max + 1))
                    .map(|x| Position { x, y: start.y }),
            );
            path.extend(
                (y_min..(y_max + 1))
                    .map(|y| Position { x: start.x, y }),
            );
        }
        for position in path {
            self.slice.insert(position, Structure::Rock);
        }
    }

    fn falls(&self, sand: &Position) -> Sand {
        let mut structures_below: Vec<usize> = self
            .slice
            .keys()
            .filter_map(|position| {
                if sand.x == position.x && sand.y < position.y {
                    Some(position.y)
                } else {
                    None
                }
            })
            .collect();

        structures_below.push(self.floor_level);

        structures_below.sort();

        let y = structures_below[0];
        if sand.y < y - 1 {
            return Falls(Position {
                x: sand.x,
                y: y - 1,
            });
        }

        for next in [
            Position {
                x: sand.x - 1,
                y: sand.y + 1,
            },
            Position {
                x: sand.x + 1,
                y: sand.y + 1,
            },
        ] {
            if let None = self.slice.get(&next) {
                if y < self.floor_level {
                    return Falls(next);
                }
            }
        }

        return Stops;
    }

    fn drop_sand(&mut self) -> Option<()> {
        let mut sand = Position { x: 500, y: 0 };
        loop {
            match self.falls(&sand) {
                Voids => return None,
                Stops => {
                    return match self.slice.insert(sand, Structure::Sand) {
                        None => Some(()),
                        Some(_) => None,
                    }
                }
                Falls(s) => {
                    sand = Position { x: s.x, y: s.y };
                }
            }
        }
    }
}

impl Display for Scan {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let max_x = self.slice.keys().map(|p| p.x).max().unwrap();
        let min_x = self.slice.keys().map(|p| p.x).min().unwrap();
        let max_y = self.floor_level;
        let min_y = 0;

        let map: String = (min_y..(max_y + 1))
            .map(|y| {
                let mut line = (min_x..(max_x + 1))
                    .map(|x| {
                        if x == 500 && y == 0 {
                            return '+';
                        }

                        if y == self.floor_level {
                            return '#';
                        }

                        match self.slice.get(&Position { x, y }) {
                            Some(Structure::Rock) => '#',
                            Some(Structure::Sand) => 'O',
                            _ => '.',
                        }
                    })
                    .collect::<Vec<_>>();
                line.push('\n');
                line
            })
            .flatten()
            .collect();
        write!(f, "{}", map)
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut scan = Scan::new();
    for line in lines {
        let points = line
            .split(" -> ")
            .map(|point| Position::new(point))
            .collect::<Vec<_>>();
        scan.add_path(points);
    }

    println!("{}", scan);

    let mut drops = 0;
    while let Some(_) = scan.drop_sand() {
        drops += 1;
    }

    println!("{}", scan);
    println!("{}", drops);
}
