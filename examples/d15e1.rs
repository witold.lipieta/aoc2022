use aoc::get_input;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::ops::Range;

struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(input: &str) -> Self {
        let (xs, ys) = input.split_once(", ").unwrap();
        let x = xs.trim_start_matches("x=").parse().unwrap();
        let y = ys.trim_start_matches("y=").parse().unwrap();
        Point { x, y }
    }

    fn distance(&self, other: &Point) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

fn merge_ranges(first: &Range<i32>, second: &Range<i32>) -> Option<Range<i32>> {
    if second.start <= first.end && second.end >= first.start {
        Some((first.start.min(second.start))..(first.end.max(second.end)))
    } else {
        None
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    // let y = 10;
    let y = 2000000;
    let mut beacons_in_line: HashSet<i32> = HashSet::new();
    let mut ranges: VecDeque<Range<i32>> = VecDeque::new();

    for line in lines {
        let (sensor_s, beacon_s) = line
            .trim_start_matches("Sensor at ")
            .split_once(": closest beacon is at ")
            .unwrap();
        let sensor = Point::new(sensor_s);
        let beacon = Point::new(beacon_s);
        let no_beacon_in = sensor.distance(&beacon);
        let distance_from_y = (y - sensor.y).abs();
        if distance_from_y < no_beacon_in {
            let covered = no_beacon_in - distance_from_y;
            let range = (sensor.x - covered)..(sensor.x + covered + 1);
            ranges.push_back(range);
        }
        if beacon.y == y {
            beacons_in_line.insert(beacon.x);
        }
    }

    'outer: loop {
        let range = ranges.pop_front().unwrap();
        if ranges.is_empty() {
            ranges.push_back(range);
            break;
        }

        for _ in 0..(ranges.len()) {
            if let Some(other_range) = ranges.pop_front() {
                if let Some(new_range) = merge_ranges(&range, &other_range) {
                    ranges.push_front(new_range);
                    continue 'outer;
                } else {
                    ranges.push_back(other_range);
                }
            }
        }

        ranges.push_back(range);
        if ranges.len() == 2 {
            break;
        }
    }

    let res: usize = ranges.iter().map(|r| r.len()).sum();
    println!("{:?}", res - beacons_in_line.len());
}
