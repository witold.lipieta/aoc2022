use aoc::get_input;
use std::collections::{BTreeSet, HashMap};

struct Filesystem {
    dir_sizes: HashMap<String, i64>,
    current_dir: String,
}

impl Filesystem {
    fn new() -> Self {
        Self {
            dir_sizes: HashMap::from([("/".to_string(), 0)]),
            current_dir: "/".to_string(),
        }
    }

    fn parent(&self, path: &str) -> Option<String> {
        match path {
            "/" => None,
            path =>  Some(path[..path.rfind("/")?].to_string()),
        }
    }

    fn cd(&mut self, directory: &str) {
        self.current_dir = match directory {
            ".." => self.parent(self.current_dir.as_str()).unwrap(),
            "/" => "/".to_string(),
            d => format!("{}/{}", self.current_dir, d),
        };
    }

    fn ls(&mut self, fstype: &str, name: &str) {
        match fstype {
            "dir" => {
                let path = format!("{}/{}", self.current_dir, name);
                self.dir_sizes.insert(path, 0);
            }
            size => {
                let size = size.parse::<i64>().unwrap();
                self.update_filesystem_sizes(self.current_dir.clone(), size);
            }
        };
    }

    fn update_filesystem_sizes(&mut self, mut path: String, size: i64) {
        loop {
            self.dir_sizes.get_mut(&path).and_then(|s| {
                *s += size;
                Some(())
            });
            path = match self.parent(path.as_str()) {
                None => break,
                Some(p) => p.clone(),
            }
        }
    }

    fn find_smallest_to_delete(&self) -> Option<i64> {
        let needed_space = 30000000 - (70000000 - *self.dir_sizes.get("/").unwrap());
        self.dir_sizes
            .values()
            .filter_map(|v| (*v > needed_space).then(|| *v))
            .collect::<BTreeSet<i64>>()
            .iter()
            .rev()
            .last()
            .cloned()
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut filesystem = Filesystem::new();

    let mut i = lines.iter().peekable();
    while let Some(line) = i.next() {
        if line.starts_with("$ cd") {
            filesystem.cd(line.split_whitespace().last().unwrap());
        } else if line.starts_with("$ ls") {
            while i.peek().map(|l| (!l.starts_with("$"))).unwrap_or(false) {
                let ls_output: Vec<&str> = i.next().unwrap().split_whitespace().collect();
                filesystem.ls(ls_output[0], ls_output[1]);
            }
        }
    }

    println!("{:?}", filesystem.find_smallest_to_delete().unwrap());
}
