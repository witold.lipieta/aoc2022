use std::cmp::Ordering;
use aoc::get_input;

#[derive(Eq, PartialEq, Clone)]
enum Packet {
    Integer(i32),
    List(Vec<Packet>),
}

impl Packet {
    fn new(input: &str) -> Self {
        let mut brackets = input.match_indices("[").collect::<Vec<_>>();
        brackets.extend(input.match_indices("]").collect::<Vec<_>>());
        brackets.sort_by_key(|(i, _)| *i);

        let mut pos = 0;
        let mut lev: Vec<Vec<Packet>> = Vec::new();
        for (i, bracket) in brackets {
            let data = &input[pos..i].split(",").filter(|s| !s.is_empty()).collect::<Vec<_>>();
            pos = i+1;
            let data = data.iter()
                .map(|s| {
                    Packet::Integer(s.parse().unwrap())
                })
                .collect::<Vec<_>>();
            if !data.is_empty() {
                lev.last_mut().unwrap().extend(data);
            }
            match bracket {
                "[" => lev.push(Vec::new()),
                "]" => {
                    if lev.len() > 1 {
                        let packet = Packet::List(lev.pop().unwrap());
                        lev.last_mut().unwrap().push(packet);
                    }
                },
                _ => {},
            };
        }
        Packet::List(lev.pop().unwrap())
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Packet::Integer(s), Packet::Integer(o)) => {
                return Some(s.cmp(o));
            },
            (Packet::List(ls), Packet::List(lo)) => {
                let common = ls.len().min(lo.len());
                for i in 0..common {
                    match ls[i].partial_cmp(&lo[i]) {
                        None => {}
                        Some(Ordering::Equal) => {},
                        Some(ordering) => return Some(ordering),
                    }
                }
                return Some(ls.len().cmp(&lo.len()));
            },
            (Packet::Integer(i), Packet::List(_)) => {
                return Packet::List(Vec::from([Packet::Integer(*i)])).partial_cmp(other);
            },
            (Packet::List(_), Packet::Integer(i)) => {
                return self.partial_cmp(&Packet::List(Vec::from([Packet::Integer(*i)])));
            },
        }
    }
}

impl Ord for Packet {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut packets: Vec<Packet> = lines
        .iter()
        .filter(|l| !l.is_empty())
        .map(|l| Packet::new(l))
        .collect();

    let p1 = Packet::List(Vec::from([Packet::List(Vec::from([Packet::Integer(2)]))]));
    let p2 = Packet::List(Vec::from([Packet::List(Vec::from([Packet::Integer(6)]))]));
    packets.push(p1.clone());
    packets.push(p2.clone());

    packets.sort();

    let i1 = packets.iter().position(|p| p.eq(&p1)).unwrap() + 1;
    let i2 = packets.iter().position(|p| p.eq(&p2)).unwrap() + 1;
    println!("{:?}", i1 * i2);
}
