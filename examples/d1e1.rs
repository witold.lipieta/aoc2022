use aoc::get_input;

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut calories = 0;
    let mut most_calories = 0;

    for line in lines {
        if line.is_empty() {
            calories = 0;
        } else {
            calories += line.parse::<i32>().unwrap();
            if calories > most_calories {
                most_calories = calories;
            }
        }
    }

    println!("{}", most_calories);
}
