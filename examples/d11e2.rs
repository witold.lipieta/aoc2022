use aoc::get_input;
use std::str::FromStr;

struct Monkey {
    items: Vec<i64>,
    operation: Operation,
    test: i64,
    throw_on_pass: usize,
    throw_on_fail: usize,
    inspections: i64,
}

enum Operator {
    Add,
    Multiply,
}

impl FromStr for Operator {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "+" => Ok(Operator::Add),
            "*" => Ok(Operator::Multiply),
            _ => Err(()),
        }
    }
}

enum Operand {
    Old,
    Number(i64),
}

impl FromStr for Operand {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "old" => Ok(Operand::Old),
            n => match n.parse::<i64>() {
                Ok(n) => Ok(Operand::Number(n)),
                Err(_) => Err(()),
            },
        }
    }
}

struct Operation {
    operator: Operator,
    operand: Operand,
}

impl Operation {
    fn new(input: &str) -> Self {
        let input = input.split_whitespace().collect::<Vec<_>>();
        Operation {
            operator: input[1].parse().unwrap(),
            operand: input[2].parse().unwrap(),
        }
    }

    fn calculate(&self, old: i64) -> i64 {
        let operand = match self.operand {
            Operand::Old => old,
            Operand::Number(n) => n,
        };
        match self.operator {
            Operator::Add => old + operand,
            Operator::Multiply => old * operand,
        }
    }
}

impl Monkey {
    fn new(input: &Vec<String>) -> Self {
        let items = input[1]
            .split(": ")
            .last()
            .map(|items| {
                items
                    .split(", ")
                    .map(|s| s.parse::<i64>().unwrap())
                    .collect::<Vec<_>>()
            })
            .unwrap();
        let operation = Operation::new(input[2].split("= ").last().unwrap());
        let test = input[3].split_whitespace().last().unwrap().parse().unwrap();
        let throw_on_pass = input[4].split_whitespace().last().unwrap().parse().unwrap();
        let throw_on_fail = input[5].split_whitespace().last().unwrap().parse().unwrap();
        Monkey {
            items,
            operation,
            test,
            throw_on_pass,
            throw_on_fail,
            inspections: 0,
        }
    }

    fn process_items(&mut self, limitter: i64) -> Vec<(usize, i64)> {
        let items_to_throw = self
            .items
            .iter()
            .map(|item| {
                let worry_level = self.operation.calculate(*item);
                let worry_level = worry_level % limitter;
                if worry_level % self.test == 0 {
                    (self.throw_on_pass, worry_level)
                } else {
                    (self.throw_on_fail, worry_level)
                }
            })
            .collect::<Vec<(usize, i64)>>();

        self.inspections += self.items.len() as i64;
        self.items.clear();

        items_to_throw
    }

    fn add_item(&mut self, item: i64) {
        self.items.push(item);
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let monkey_inputs = lines
        .join("\n")
        .split("\n\n")
        .map(|mi| mi.split("\n").map(|s| s.to_string()).collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let mut monkeys = monkey_inputs
        .iter()
        .map(|s| Monkey::new(s))
        .collect::<Vec<_>>();

    let avoid_ridiculousness = monkeys.iter().map(|m| m.test).fold(1, |acc, x| acc * x);

    for _ in 0..10000 {
        for i in 0..monkeys.len() {
            for (to, item) in monkeys[i].process_items(avoid_ridiculousness) {
                monkeys[to].add_item(item);
            }
        }
    }

    let mut inspections = monkeys.iter().map(|m| m.inspections).collect::<Vec<_>>();
    inspections.sort();
    let monkey_business = inspections.iter().rev().take(2).fold(1, |acc, x| acc * x);
    println!("{:?} {:?}", inspections, monkey_business);
}
