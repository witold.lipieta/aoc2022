use aoc::get_input;
use std::collections::HashSet;
use std::hash::Hash;

const PACKET_STARTER_LENGTH: usize = 4;

struct PacketHeader<T> {
    index: usize,
    buffer: [T; PACKET_STARTER_LENGTH],
}

impl<T: Eq + Hash> PacketHeader<T> {
    fn push(&mut self, item: T) {
        self.index = (self.index + 1) % PACKET_STARTER_LENGTH;
        self.buffer[self.index] = item;
    }

    fn is_valid(&self) -> bool {
        let mut set = HashSet::new();
        self.buffer.iter().all(|x| set.insert(x))
    }
}

fn msg_start_index(line: &String) -> Option<usize> {
    let mut header: PacketHeader<char> = PacketHeader {
        buffer: ['0'; PACKET_STARTER_LENGTH],
        index: 0,
    };
    let mut index = 0;
    println!("{:?}", line);
    for ch in line.chars() {
        index += 1;
        header.push(ch);
        if index >= PACKET_STARTER_LENGTH && header.is_valid() {
            return Some(index);
        }
    }
    return None;
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    for line in &lines {
        println!("{}", msg_start_index(line).unwrap());
    }
}
