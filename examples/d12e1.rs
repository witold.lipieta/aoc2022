use aoc::get_input;
use std::collections::VecDeque;

#[derive(Copy, Clone)]
struct Position {
    x: usize,
    y: usize,
}

struct HeightMap {
    current_position: Position,
    target_position: Position,
    heights: Vec<Vec<i32>>,
}

impl HeightMap {
    fn new(input: Vec<String>) -> Self {
        let mut current_position = Position { x: 0, y: 0 };
        let mut target_position = Position { x: 0, y: 0 };
        let heights: Vec<Vec<i32>> = (0..input.len())
            .map(|r| {
                (0..input[r].len())
                    .map(|c| match input[r].as_bytes()[c] as char {
                        'S' => {
                            current_position = Position {
                                x: c as usize,
                                y: r as usize,
                            };
                            0
                        }
                        'E' => {
                            target_position = Position {
                                x: c as usize,
                                y: r as usize,
                            };
                            25
                        }
                        c => c as i32 - 97,
                    })
                    .collect::<Vec<_>>()
            })
            .collect();

        HeightMap {
            current_position,
            target_position,
            heights,
        }
    }

    fn height(&self, position: Position) -> i32 {
        self.heights[position.y][position.x]
    }

    fn floodfill(&self) -> usize {
        let mut distances: Vec<Vec<usize>> = self
            .heights
            .iter()
            .map(|r| r.iter().map(|_| 1000).collect::<Vec<_>>())
            .collect();

        let mut stack: VecDeque<(Position, usize)> = VecDeque::from([(self.target_position, 0)]);
        while !stack.is_empty() {
            let (position, distance) = stack.pop_front().unwrap();
            if distances[position.y][position.x] > distance {
                distances[position.y][position.x] = distance;
                if position.y != 0 {
                    let next_position = Position {
                        x: position.x,
                        y: position.y - 1,
                    };
                    if self.height(position) - self.height(next_position) <= 1 {
                        stack.push_back((next_position, distance + 1))
                    }
                }
                if position.x != 0 {
                    let next_position = Position {
                        x: position.x - 1,
                        y: position.y,
                    };
                    if self.height(position) - self.height(next_position) <= 1 {
                        stack.push_back((next_position, distance + 1))
                    }
                }
                if position.y != self.heights.len() - 1 {
                    let next_position = Position {
                        x: position.x,
                        y: position.y + 1,
                    };
                    if self.height(position) - self.height(next_position) <= 1 {
                        stack.push_back((next_position, distance + 1))
                    }
                }
                if position.x != self.heights[0].len() - 1 {
                    let next_position = Position {
                        x: position.x + 1,
                        y: position.y,
                    };
                    if self.height(position) - self.height(next_position) <= 1 {
                        stack.push_back((next_position, distance + 1))
                    }
                }
            }
        }

        distances[self.current_position.y][self.current_position.x]
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let height_map = HeightMap::new(lines);

    println!("{:?}", height_map.floodfill());
}
