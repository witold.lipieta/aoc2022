use aoc::get_input;
use std::collections::HashSet;
use std::hash::Hash;
use std::str::FromStr;

enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "U" => Ok(Direction::Up),
            "R" => Ok(Direction::Right),
            "D" => Ok(Direction::Down),
            "L" => Ok(Direction::Left),
            _ => Err(()),
        }
    }
}

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
struct Position {
    x: i32,
    y: i32,
}

trait Knot {
    fn position(&self) -> Position;
}

struct Head {
    position: Position,
}

impl Head {
    fn step(&mut self, direction: &Direction) {
        match direction {
            Direction::Up => self.position.y += 1,
            Direction::Right => self.position.x += 1,
            Direction::Down => self.position.y -= 1,
            Direction::Left => self.position.x -= 1,
        }
    }
}

impl Knot for Head {
    fn position(&self) -> Position {
        self.position.clone()
    }
}

struct Tail {
    position: Position,
    visited: HashSet<Position>,
}

impl Tail {
    fn new(position: Position) -> Self {
        Tail {
            position: position.clone(),
            visited: HashSet::from([position]),
        }
    }

    fn follow(&mut self, head: &impl Knot) {
        let head_position = head.position();
        let x_difference = head_position.x - self.position.x;
        let y_difference = head_position.y - self.position.y;

        if x_difference.abs() <= 1 && y_difference.abs() <= 1 {
            return;
        }

        if (x_difference.abs() > 1 && y_difference.abs() != 0)
            || (x_difference.abs() != 0 && y_difference.abs() > 1)
        {
            self.position.x += x_difference.signum();
            self.position.y += y_difference.signum();
        } else if x_difference.abs() > 1 {
            self.position.x += x_difference.signum();
        } else if y_difference.abs() > 1 {
            self.position.y += y_difference.signum();
        }

        self.visited.insert(self.position.clone());
    }
}

impl Knot for Tail {
    fn position(&self) -> Position {
        self.position.clone()
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let start = Position { x: 0, y: 0 };

    let mut head = Head {
        position: start.clone(),
    };
    let mut k1 = Tail::new(start.clone());
    let mut k2 = Tail::new(start.clone());
    let mut k3 = Tail::new(start.clone());
    let mut k4 = Tail::new(start.clone());
    let mut k5 = Tail::new(start.clone());
    let mut k6 = Tail::new(start.clone());
    let mut k7 = Tail::new(start.clone());
    let mut k8 = Tail::new(start.clone());
    let mut tail = Tail::new(start);

    for line in &lines {
        let move_instruction: Vec<&str> = line.split_whitespace().collect();
        let direction = Direction::from_str(move_instruction[0]).unwrap();
        let times: i32 = move_instruction[1].parse().unwrap();

        for _ in 0..times {
            head.step(&direction);
            k1.follow(&head);
            k2.follow(&k1);
            k3.follow(&k2);
            k4.follow(&k3);
            k5.follow(&k4);
            k6.follow(&k5);
            k7.follow(&k6);
            k8.follow(&k7);
            tail.follow(&k8);
        }
    }

    println!("{:?}", tail.visited.len());
}
