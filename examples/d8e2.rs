use aoc::get_input;

struct TreeGrid {
    grid: Vec<Vec<u8>>,
}

impl TreeGrid {
    fn new() -> Self {
        TreeGrid { grid: Vec::new() }
    }

    fn add_line(&mut self, line: &String) {
        let line: Vec<u8> = line
            .chars()
            .map(|c| c.to_digit(10).unwrap() as u8)
            .collect();
        self.grid.push(line);
    }

    fn scenic_score(&self, r: usize, c: usize) -> i32 {
        let height = self.grid[r][c];
        let mut score_u = 0;
        for j in (0..r).rev() {
            score_u += 1;
            if self.grid[j][c] >= height {
                break;
            }
        }

        let mut score_d = 0;
        for j in (r + 1)..(self.grid.len()) {
            score_d += 1;
            if self.grid[j][c] >= height {
                break;
            }
        }

        let mut score_r = 0;
        for i in (c + 1)..(self.grid[r].len()) {
            score_r += 1;
            if self.grid[r][i] >= height {
                break;
            }
        }

        let mut score_l = 0;
        for i in (0..c).rev() {
            score_l += 1;
            if self.grid[r][i] >= height {
                break;
            }
        }
        score_u * score_d * score_r * score_l
    }

    fn highest_scenic_score(&self) -> i32 {
        let mut highest_score = 0;
        for r in 1..(self.grid.len() - 1) {
            for c in 1..(self.grid[0].len() - 1) {
                let score = self.scenic_score(r, c);
                if score > highest_score {
                    highest_score = score;
                }
            }
        }
        highest_score
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut tree_grid = TreeGrid::new();

    for line in &lines {
        tree_grid.add_line(line);
    }

    println!("{:?}", tree_grid.highest_scenic_score());
}
