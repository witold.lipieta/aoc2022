use aoc::get_input;
use std::collections::BTreeMap;

struct Register {
    cycle: i32,
    value: i32,
    history: BTreeMap<i32, i32>,
}

impl Register {
    fn new() -> Self {
        let cycle = 0;
        let value = 1;
        Register {
            cycle,
            value,
            history: BTreeMap::from([(cycle, value)]),
        }
    }

    fn parse_instruction(&mut self, line: String) {
        match line.as_str() {
            "noop" => self.cycle += 1,
            l => {
                let value: i32 = l.split_whitespace().last().unwrap().parse().unwrap();
                self.cycle += 2;
                self.value += value;

                self.history.insert(self.cycle, self.value);
            }
        };
    }

    fn register_at_cycle(&self, cycle: i32) -> i32 {
        self.history
            .iter()
            .take_while(|(k, _)| **k < cycle)
            .last()
            .map(|(_, v)| *v)
            .unwrap()
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut register = Register::new();

    for line in lines {
        register.parse_instruction(line);
    }

    let _ = (1..241)
        .map(|cycle| {
            let symbol = match (((cycle - 1) % 40) - register.register_at_cycle(cycle)).abs() <= 1 {
                true => "#",
                false => " ",
            };
            print!("{}", symbol);
            if (cycle % 40) == 0 {
                println!();
            }
        })
        .collect::<Vec<_>>();
}
