use aoc::get_input;

struct TreeGrid {
    grid: Vec<Vec<u8>>,
}

impl TreeGrid {
    fn new() -> Self {
        TreeGrid { grid: Vec::new() }
    }

    fn add_line(&mut self, line: &String) {
        let line: Vec<u8> = line
            .chars()
            .map(|c| c.to_digit(10).unwrap() as u8)
            .collect();
        self.grid.push(line);
    }

    fn visibility(&self) -> i32 {
        let mut visibility: Vec<Vec<bool>> = Vec::new();
        for j in 0..self.grid.len() {
            visibility.push(Vec::new());
            for i in 0..self.grid[0].len() {
                match (j, i) {
                    (0, _) | (_, 0) => visibility[j].push(true),
                    (l, _) if l == self.grid.len() - 1 => visibility[j].push(true),
                    (_, l) if l == self.grid[0].len() - 1 => visibility[j].push(true),
                    (_, _) => visibility[j].push(false),
                }
            }
        }

        for i in 1..(self.grid[0].len() - 1) {
            let mut max_visible = self.grid[0][i];
            for j in 1..(self.grid.len() - 1) {
                let visible = self.grid[j][i] > max_visible;
                if visible {
                    visibility[j][i] = true;
                    max_visible = self.grid[j][i]
                }
            }
        }

        for i in 1..(self.grid[0].len() - 1) {
            let mut max_visible = self.grid[self.grid.len() - 1][i];
            for j in (1..(self.grid.len() - 1)).rev() {
                let visible = self.grid[j][i] > max_visible;
                if visible {
                    visibility[j][i] = true;
                    max_visible = self.grid[j][i];
                }
            }
        }

        for j in 1..(self.grid.len() - 1) {
            let mut max_visible = self.grid[j][0];
            for i in 1..(self.grid[0].len() - 1) {
                let visible = self.grid[j][i] > max_visible;
                if visible {
                    visibility[j][i] = true;
                    max_visible = self.grid[j][i];
                }
            }
        }

        for j in 1..(self.grid.len() - 1) {
            let mut max_visible = self.grid[j][self.grid[j].len() - 1];
            for i in (1..(self.grid[0].len() - 1)).rev() {
                let visible = self.grid[j][i] > max_visible;
                if visible {
                    visibility[j][i] = true;
                    max_visible = self.grid[j][i];
                }
            }
        }

        let a: i32 = visibility
            .iter()
            .map(|v| v.iter().map(|b| *b as i32).sum::<i32>())
            .sum();
        a
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut tree_grid = TreeGrid::new();

    for line in &lines {
        tree_grid.add_line(line);
    }

    println!("{:?}", tree_grid.visibility());
}
