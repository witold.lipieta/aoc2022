use aoc::get_input;
use std::collections::VecDeque;
use std::ops::Range;

struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(input: &str) -> Self {
        let (xs, ys) = input.split_once(", ").unwrap();
        let x = xs.trim_start_matches("x=").parse().unwrap();
        let y = ys.trim_start_matches("y=").parse().unwrap();
        Point { x, y }
    }

    fn distance(&self, other: &Point) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

fn merge_ranges(first: &Range<i32>, second: &Range<i32>) -> Option<Range<i32>> {
    if second.start <= first.end && second.end >= first.start {
        Some((first.start.min(second.start))..(first.end.max(second.end)))
    } else {
        None
    }
}


fn difference(first: &Range<i32>, second: &Range<i32>) -> Option<Range<i32>> {
    // as there is exactly one point viable for distress beacon,
    // it is enough to return EITHER prefix or suffix difference
    if second.start > first.start {
        Some((first.start)..(second.start.min(first.end)))
    } else if second.end < first.end {
        Some((first.start.max(second.end))..(first.end))
    } else {
        None
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut ranges: Vec<VecDeque<Range<i32>>> = Vec::new();

    // let ys = 0..21;
    let ys = 0..4000001;
    for _ in ys.clone() {
        ranges.push(VecDeque::new());
    }

    for line in lines {
        let (sensor_s, beacon_s) = line
            .trim_start_matches("Sensor at ")
            .split_once(": closest beacon is at ")
            .unwrap();
        let sensor = Point::new(sensor_s);
        let beacon = Point::new(beacon_s);
        let no_beacon_in = sensor.distance(&beacon);
        for y in ys.clone() {
            let distance_from_y = (y - sensor.y).abs();
            if distance_from_y < no_beacon_in {
                let covered = no_beacon_in - distance_from_y;
                let range = (sensor.x - covered)..(sensor.x + covered + 1);
                ranges[y as usize].push_back(range);
            }
        }
    }

    let mut res_x: usize = 0;
    let mut res_y = 0;

    for i in 0..(ranges.len()) {
        let r = &mut ranges[i];
        'outer: loop {
            let range = r.pop_front().unwrap();
            if r.is_empty() {
                r.push_back(range);
                break;
            }

            for _ in 0..(r.len()) {
                if let Some(other_range) = r.pop_front() {
                    if let Some(new_range) = merge_ranges(&range, &other_range) {
                        r.push_back(new_range);
                        continue 'outer;
                    } else {
                        r.push_back(other_range);
                    }
                }
            }

            r.push_back(range);
            if r.len() == 2 {
                break;
            }
        }

        if let Some(relative_complement) = difference(&ys, &r[0]) {
            res_y = i;
            if r.len() > 1 {
                if let Some(res) = difference(&relative_complement, &r[1]) {
                    res_x = res.collect::<Vec<_>>()[0] as usize;
                    break;
                }
            } else {
                res_x = relative_complement.collect::<Vec<_>>()[0] as usize;
                break;
            }
        }
    }

    println!("{}", res_x * 4000000 + res_y);
}
