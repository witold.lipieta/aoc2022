use aoc::get_input;
use std::collections::HashSet;
use std::hash::Hash;

#[derive(Hash, Eq, PartialEq, Clone)]
struct Content {
    i: char,
}

struct Rucksack {
    c1: HashSet<Content>,
    c2: HashSet<Content>,
}

impl Rucksack {
    fn new(stored: String) -> Self {
        let half = stored.len() / 2;

        Rucksack {
            c1: stored[..half].chars().map(|c| Content { i: c }).collect(),
            c2: stored[half..].chars().map(|c| Content { i: c }).collect(),
        }
    }

    fn common_item(&self) -> Option<Content> {
        self.c1.intersection(&self.c2).last().map(|c| c.clone())
    }
}

impl Content {
    fn priority(&self) -> i32 {
        let priority = self.i as u32;

        if priority > 96 {
            priority as i32 - 96
        } else {
            priority as i32 - 38
        }
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut priorities = 0;

    for line in lines {
        priorities += Rucksack::new(line).common_item().unwrap().priority();
    }

    println!("{:?}", priorities);
}
