use aoc::get_input;
use std::collections::HashSet;
use std::hash::Hash;

#[derive(Hash, Eq, PartialEq, Clone)]
struct Content {
    i: char,
}

struct ElvGroup {
    r1: HashSet<Content>,
    r2: HashSet<Content>,
    r3: HashSet<Content>,
}

impl ElvGroup {
    fn new(r1: &String, r2: &String, r3: &String) -> Self {
        ElvGroup {
            r1: r1.chars().map(|c| Content { i: c }).collect(),
            r2: r2.chars().map(|c| Content { i: c }).collect(),
            r3: r3.chars().map(|c| Content { i: c }).collect(),
        }
    }

    fn common_item(&self) -> Option<Content> {
        self.r1
            .intersection(&self.r2)
            .map(|c| c.clone())
            .collect::<HashSet<Content>>()
            .intersection(&self.r3)
            .last()
            .map(|c| c.clone())
    }
}

impl Content {
    fn priority(&self) -> i32 {
        let priority = self.i as u32;

        if priority > 96 {
            priority as i32 - 96
        } else {
            priority as i32 - 38
        }
    }
}

fn main() {
    let lines: Vec<String> = get_input().unwrap();

    let mut priorities = 0;

    for i in (0..lines.len()).step_by(3) {
        priorities += ElvGroup::new(&lines[i], &lines[i + 1], &lines[i + 2])
            .common_item()
            .unwrap()
            .priority();
    }

    println!("{:?}", priorities);
}
