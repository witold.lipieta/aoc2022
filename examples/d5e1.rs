use aoc::get_input;

#[derive(Debug)]
struct Instruction {
    count: usize,
    from: usize,
    to: usize,
}

impl Instruction {
    fn new(line: &String) -> Self {
        let instruction: Vec<&str> = line.split_whitespace().collect();
        let count = instruction[1].parse::<usize>().unwrap();
        let from = instruction[3].parse::<usize>().unwrap() - 1;
        let to = (instruction[5]).parse::<usize>().unwrap() - 1;
        Self { count, from, to }
    }
}

struct Supplies<T> {
    stacks: Vec<Vec<T>>,
}

impl<T: Copy> Supplies<T> {
    fn new() -> Self {
        Self { stacks: Vec::new() }
    }

    fn add_stack(&mut self) {
        self.stacks.push(Vec::new());
    }

    fn add_cargo(&mut self, stack: usize, cargo: T) {
        self.stacks[stack].push(cargo);
    }

    fn move_cargo(&mut self, instruction: &Instruction) {
        let cargo: Vec<T> = (0..instruction.count)
            .map(|_| self.stacks[instruction.from].pop().unwrap())
            .collect();
        self.stacks[instruction.to].extend(cargo);
    }

    fn tops(&self) -> Vec<T> {
        self.stacks
            .iter()
            .map(|s| s.last().copied().unwrap())
            .collect()
    }
}

fn main() {
    let mut lines: Vec<String> = get_input().unwrap();
    lines.reverse();

    let mut supplies = Supplies::<char>::new();
    let mut moves: Vec<Instruction> = Vec::new();

    for line in &lines {
        if line.starts_with("move") {
            moves.push(Instruction::new(&line));
        } else if line.starts_with(" 1") {
            for _ in line.split_whitespace() {
                supplies.add_stack();
            }
        } else if !line.is_empty() {
            for (i, _) in line.match_indices("[") {
                supplies.add_cargo(i / 4, line.as_bytes()[i + 1] as char);
            }
        }
    }

    moves.reverse();
    for instruction in &moves {
        supplies.move_cargo(instruction);
    }

    println!("{}", supplies.tops().into_iter().collect::<String>());
}
